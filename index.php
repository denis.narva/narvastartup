<?php
if (isset($_GET['lang'])) {
    switch($_GET['lang']) :
        default: include('lang/rus.php'); break;
        case 'est': include('lang/est.php'); break;
        case 'eng': include('lang/eng.php'); break;
    endswitch;
} else {
    include('lang/rus.php');
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Narva startup</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	
	<!-- Font -->
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
	
	
	<link href="common-css/ionicons.css" rel="stylesheet">
	
	
	<link rel="stylesheet" href="common-css/jquery.classycountdown.css" />
		
	<link href="styles/css/styles.css" rel="stylesheet">
	
	<link href="styles/css/responsive.css" rel="stylesheet">
	
</head>
<body>
	
	<div class="main-area center-text bg-fon" style="background-image:url(images/picture.jpg);">
        <div style="text-align: right; margin-right: 9px;">
            <a href="?lang=rus">
                rus
            </a>
            <a href="?lang=est">
                est
            </a>
            <a href="?lang=eng">
                eng
            </a>
        </div>
		
		<div class="display-table">
			<div class="display-table-cell">
				
				<h1 class="title font-white"><b><?php echo $lang['text']; ?></b></h1>

				<ul class="social-btn font-white">
					<li><a href="https://www.facebook.com/startupnarva.ee" target="_blank">facebook</a></li>
					<li><a href="https://www.instagram.com/startupnarva.ee/" target="_blank">instagram</a></li>
				</ul><!-- social-btn -->
				
			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->
	
</body>
</html>